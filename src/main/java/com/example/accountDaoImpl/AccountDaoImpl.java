package com.example.accountDaoImpl;

import org.jongo.Jongo;
import org.springframework.stereotype.Repository;

import com.example.accountDao.AccountDao;
import com.example.mongoUtil.MongoUtill;
import com.example.vo.BaseResponse;
import com.example.vo.ProfileInfo;

@Repository("accountDao")
public class AccountDaoImpl implements AccountDao {

	@Override
	public BaseResponse registration(ProfileInfo profileInfo) {
		
		BaseResponse baseResponse=new BaseResponse();
		baseResponse.setStatusCode(200);
		baseResponse.setStatusMesssage("success");
		ProfileInfo info=	new Jongo(MongoUtill.getDB()).getCollection("EmployeeRegistration").findOne("{contactInfo:#}",profileInfo.getContactInfo()).as(ProfileInfo.class);
		if(info==null) {
		new Jongo(MongoUtill.getDB()).getCollection("EmployeeRegistration").insert(profileInfo);
		
		}
		else {
			baseResponse.setStatusCode(401);
			baseResponse.setStatusMesssage("Already Exits");
		}	
		return baseResponse;
	}

}
