package com.example.accountDao;

import com.example.vo.BaseResponse;
import com.example.vo.ProfileInfo;

public interface AccountDao {

	BaseResponse registration(ProfileInfo profileInfo);

}
