package com.example.accountController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.accountService.AccountService;
import com.example.vo.BaseResponse;
import com.example.vo.ProfileInfo;

@RestController
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	AccountService accountService;
	
	@RequestMapping(value = "/hello", method = RequestMethod.POST, headers = "Accept=application/json")
	public String hello() {
		return "hello";
	}
	@RequestMapping(value="/registration",method=RequestMethod.POST,headers="Accept=application/json")
public BaseResponse registration(@RequestBody ProfileInfo profileInfo) {
	return	accountService.registration(profileInfo);
	}	
	
	

}
