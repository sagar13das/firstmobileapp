package com.example.accountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.accountDao.AccountDao;
import com.example.vo.BaseResponse;
import com.example.vo.ProfileInfo;

@Service("accountService")
public class AccountService {
	@Autowired
	AccountDao accountDao;

	public BaseResponse registration(ProfileInfo profileInfo) {
	return	accountDao.registration(profileInfo);

	}

}
